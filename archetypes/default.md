---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ now.Format "2006-01-01" }}
date_start: {{ now.Format "2006-01-01" }}
sub_title: ""
description: ""
techniques: ""
dimensions: ""
tags: []
vimeo:
youtube:
cover_video: false
git:
wiki:
link:
image:
image_home:
txt_color: white
txt_color_home: white
weight:
---

