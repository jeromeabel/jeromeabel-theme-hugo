// https://www.w3schools.com/howto/howto_js_scroll_to_top.asp
// Get the button
let scrollButton = document.getElementById("scroll-btn");
let scrollValue = 1000;

// When the user scrolls down from the top of the document, show the button
window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  let threshold1 = document.body.scrollHeight - scrollValue;
  let threshold2 = document.documentElement.scrollHeight - scrollValue;
  if (
   // document.body.scrollTop > scrollValue ||
   // document.documentElement.scrollTop > scrollValue
   document.body.scrollTop > threshold1 ||
   document.documentElement.scrollTop > threshold2
   ) 
  {
    //scrollButton.style.display = "block";
    scrollButton.classList.remove('dn');
    scrollButton.classList.add('db');
  } else {
    //scrollButton.style.display = "none";
    scrollButton.classList.remove('db');
    scrollButton.classList.add('dn');
  }
}
// When the user clicks on the button, scroll to the top of the document
scrollButton.addEventListener("click", backToTop);

function backToTop() {
  //window.scrollTo({top: 0, behavior: 'smooth'});
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}